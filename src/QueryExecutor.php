<?php

namespace Drupal\query_cache;

use QueryCache\QueryExecutorInterface;

class QueryExecutor implements QueryExecutorInterface {

  /**
   * {@inheritdoc}
   */
  public function query($query, $args, $options) {
    $result = _query_cache_execute_db_query($query, $args, $options);

    if (!is_resource($result)) {
      return $result;
    }

    // @todo Use an iterator instead.
    $data = array();
    while (($row = db_fetch_array($result))) {
      $data[] = $row;
    }

    return $data;
  }

}
