<?php

namespace Drupal\query_cache;

class CachePool {

  protected $bin;

  public function __construct($bin) {
    $this->bin = $bin;
  }

  public function getMultiple($keys) {
    $return = array();

    $items = cache_get_multiple($keys, $this->bin);
    foreach ($items as $key => $item) {
      $return[$key] = $item->data;
    }

    print_r([__FUNCTION__, $this->bin, func_get_args(), !empty($return) ? 'HIT' : 'MISS']);
    return $return;
  }

  public function set($key, $data) {
    print_r([__FUNCTION__, $this-bin, array($key, '...')]);
    cache_set($key, $data, $this->bin);
  }

  public function deleteItem($key) {
    print_r([__FUNCTION__, $this->bin, func_get_args()]);
    cache_clear_all($key, $this->bin);
  }

  public function clear() {
    print_r([__FUNCTION__, $this->bin, func_get_args()]);
    cache_clear_all('*', $this->bin, TRUE);
  }
}

