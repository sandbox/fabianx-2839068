<?php

namespace Drupal\query_cache;

class CachePoolFactory {
  
  public function get($cache_configuration) {
    return new CachePool('cache_' . $cache_configuration['pool']);
  }

}
