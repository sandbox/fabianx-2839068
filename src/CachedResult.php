<?php

namespace Drupal\query_cache;

/**
 * A class for representing a cached database query result.
 */
class CachedResult {

  protected $data;
  protected $index;
  protected $count;

  public function __construct($data = array()) {
    // @todo Use better implementation.
    if ($data instanceof \Traversable) {
      $data = iterator_to_array($data, FALSE);
    }

    $this->data = $data;

    $this->index = 0;
    $this->count = count($data);
  }

  protected function fetchRow($index) {
    $record = $this->data[$index];

    return $record;
  }

  public function fetchArray() {
    if ($this->index < $this->count) {
      $result = $this->fetchRow($this->index);
      $this->index++;
      return $result;
    }

    return FALSE;
  }

  public function fetchObject() {
    if ($this->index < $this->count) {
      return (object) $this->fetchArray();
    }

    return FALSE;
  }

  public function result() {
    if ($this->count == 0) {
      return FALSE;
    }

    $row = $this->fetchArray();

    if (!$row) {
       return FALSE;
    }

    $result = array_shift($row);

    return $result;
  }
}
