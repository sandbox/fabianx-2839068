<?php

/**
 * @file
 * Module file for the query_cache module,
 */

if (!class_exists('QueryCache\QueryCache')) {
  require_once __DIR__ . '/vendor/autoload.php';
}

require_once __DIR__ . '/src/CachedResult.php';
require_once __DIR__ . '/src/CachePoolFactory.php';
require_once __DIR__ . '/src/CachePool.php';
require_once __DIR__ . '/src/QueryExecutor.php';

use QueryCache\QueryCache;
use Drupal\query_cache\CachedResult;
use Drupal\query_cache\QueryExecutor;
use Drupal\query_cache\CachePoolFactory;

// -----------------------------------------------------------------------
// Core Hooks

/**
 * Implements hook_init().
 */
function query_cache_init() {
  global $conf;

  $init_done = &_query_cache_static(__FUNCTION__, FALSE);
  $init_done = TRUE;

  // Override functions now that init is done.
  if (empty($conf['query_cache_settings_included'])) {
    require __DIR__ . '/query_cache.settings.inc';
  }
}

/**
 * Implements hook_flush_caches().
 */
function hook_flush_caches() {
  $query_cache = query_cache();
  $pools = $query_cache->getCachePools();

  $bins = array();
  foreach ($pools as $pool) {
    $bins[] = 'cache_' . $pool;
  }

  return $bins;
}

// -----------------------------------------------------------------------
// Public API

/**
 * Returns the query cache.
 *
 * Before hook_init() this only uses the configuration
 * described in $conf, afterwards also hooks can define
 * queries / tables to be cached.
 *
 * @return \QueryCache\QueryCache
 *   The initialized query cache object.
 */
function query_cache() {
  $query_cache_data = &_query_cache_static(__FUNCTION__);

  // Instantiate the query_cache service.
  if (!isset($query_cache_data['service'])) {
    $cache_pool_factory = new CachePoolFactory();
    $query_executor = new QueryExecutor();
    $query_cache = new QueryCache($query_executor, $cache_pool_factory);
    $query_cache_data['service'] = $query_cache;
  }

  $query_cache = $query_cache_data['service'];

  // Initialize the configuration.
  if (!isset($query_cache_data['configuration'])) {
    $init_done = &_query_cache_static('query_cache_init', FALSE);

    $base_configuration = variable_get('query_cache', array());
    if ($init_done) {
      $base_configuration += module_invoke_all('query_cache_info');
      drupal_alter('query_cache_info', $base_configuration);
    }
    else {
      $query_cache_data = &_query_cache_static(__FUNCTION__ . ':before_init');
      $query_cache_data['service'] = $query_cache;

      if (isset($query_cache_data['configuration'])) {
        return $query_cache;
      }
    }

    $query_cache->setConfiguration($base_configuration);
    $query_cache_data['configuration'] = TRUE;
  }

  return $query_cache;
}

// -----------------------------------------------------------------------
// Database API

/**
 * Overrides db_query().
 */
function query_cache_db_query($query) {
  $args = func_get_args();
  array_shift($args);

  $query_cache = query_cache();

  if (variable_get('query_cache_debug', false)) {
    error_log(print_r(['__QUERY__', $query, $args], TRUE));
  }

  $data = $query_cache->query($query, $args, array());
  return new CachedResult($data);
}

/**
 * Overrides db_query_range().
 */
function query_cache_db_query_range($query) {
  $args = func_get_args();
  $count = array_pop($args);
  $from = array_pop($args);
  array_shift($args);

  return _query_cache_execute_db_query_range($query, $args, $from, $count);
}

/**
 * Overrides db_fetch_array().
 */
function query_cache_db_fetch_array($result) {
  if ($result instanceof CachedResult) {
    return $result->fetchArray();
  }

  return _query_cache_execute_original_function('db_fetch_array', array($result));
}

/**
 * Overrides db_fetch_object().
 */
function query_cache_db_fetch_object($result) {
  if ($result instanceof CachedResult) {
    return $result->fetchObject();
  }

  return _query_cache_execute_original_function('db_fetch_object', array($result));
}

/**
 * Overrides db_fetch_result().
 */
function query_cache_db_result($result) {
  if ($result instanceof CachedResult) {
    return $result->result();
  }

  return _query_cache_execute_original_function('db_result', array($result));
}

// -----------------------------------------------------------------------
// Private API

/**
 * Simplified backport of drupal_static().
 */
function &_query_cache_static($name, $default_value = NULL, $reset = FALSE) {
  static $data = array();

  if ($reset) {
    if (isset($name)) {
      unset($data[$name]);
      return;
    }

    $data = array();
    return;
  }

  if (!isset($data[$name]) && !array_key_exists($name, $data)) {
    $data[$name] = $default_value;
  }

  return $data[$name];
}

/**
 * Execute a db_query() using the original function.
 *
 * @param string $query
 * @param array $args
 * @param array $options
 */
function _query_cache_execute_db_query($query, $args, $options = array()) {
  array_unshift($args, $query);
  return _query_cache_execute_original_function('db_query', $args);
}

/**
 * Execute a db_query_range() using the original function.
 *
 * @param string $query
 * @param arrag $args
 */
function _query_cache_execute_db_query_range($query, $args, $from, $count, $options = array()) {
  array_unshift($args, $query);
  array_push($args, $from);
  array_push($args, $count);
  return _query_cache_execute_original_function('db_query_range', $args);
}

/**
 * Execute the original version of a function.
 *
 * @param string $function
 *   The original function name.
 * @param array $args
 *   The arguments to pass to the function.
 */
function _query_cache_execute_original_function($function, $args) {
  global $conf;

  $variable = $function . '_function';
  $old_function = $conf[$variable];
  $conf[$variable] = FALSE;

  $result = call_user_func_array($function, $args);

  $conf[$variable] = $old_function;

  return $result;
}
