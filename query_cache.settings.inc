<?php

$conf['db_query_function'] = 'query_cache_db_query';
$conf['db_query_range_function'] = 'query_cache_db_query_range';
$conf['db_fetch_array_function'] = 'query_cache_db_fetch_array';
$conf['db_fetch_object_function'] = 'query_cache_db_fetch_object';
$conf['db_result_function'] = 'query_cache_db_result';

// Ensure we don't set settings twice.
$conf['query_cache_settings_included'] = TRUE;
